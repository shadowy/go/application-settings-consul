module gitlab.com/shadowy/go/application-settings-consul

go 1.12

require (
	github.com/hashicorp/consul/api v1.1.0
	github.com/sirupsen/logrus v1.4.2
	gitlab.com/shadowy/go/application-settings v1.0.10
)
