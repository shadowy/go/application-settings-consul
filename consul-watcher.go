package watcher

import (
	"github.com/hashicorp/consul/api"
	"github.com/sirupsen/logrus" //nolint:depguard
	"gitlab.com/shadowy/go/application-settings/settings"
	"time"
)

// ConsulWatcher - watcher setting in consul server
type ConsulWatcher struct {
	consul         *api.Client
	key            string
	updateInterval int
	lastIndex      uint64

	config  *settings.Configuration
	creator settings.WatcherSettingsCreator
}

// NewConsulWatcher - create new consul watcher
func NewConsulWatcher(consul *api.Client, key string) *ConsulWatcher {
	result := ConsulWatcher{consul: consul, key: key}
	return &result
}

// Setup - setup settings for watcher
func (watcher *ConsulWatcher) Setup(cfg *settings.Configuration, creator settings.WatcherSettingsCreator) (err error) {
	logrus.Debug("FileWatcher.Setup")
	watcher.config = cfg
	watcher.creator = creator

	watcher.read()

	go func() {
		ticker := time.Tick(time.Second * 3)
		for range ticker {
			watcher.read()
		}
	}()

	return
}

func (watcher *ConsulWatcher) read() {
	kv := watcher.consul.KV()
	p, v, err := kv.Get(watcher.key, nil)
	if err != nil {
		logrus.WithError(err).WithFields(watcher.params()).Error("ConsulWatcher.read")
		return
	}
	if v.LastIndex == watcher.lastIndex {
		return
	}
	watcher.lastIndex = v.LastIndex
	cfg, err := watcher.creator(string(p.Value))
	if err != nil {
		logrus.WithFields(watcher.params()).WithError(err).Error("ConsulWatcher.read creator")
		return
	}
	logrus.WithFields(watcher.params()).Debug("ConsulWatcher.read set")
	watcher.config.Set(cfg)
}

func (watcher ConsulWatcher) params() logrus.Fields {
	return logrus.Fields{"key": watcher.key, "index": watcher.lastIndex, "updateInterval": watcher.updateInterval}
}
