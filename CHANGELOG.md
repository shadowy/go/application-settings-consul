<a name="unreleased"></a>
## [Unreleased]


<a name="v1.0.6"></a>
## [v1.0.6] - 2019-08-29
### Bug Fixes
- issue with re-reading setting from consul


<a name="v1.0.5"></a>
## [v1.0.5] - 2019-08-05
### Bug Fixes
- fix issue with initiate consul client in watcher


<a name="v1.0.4"></a>
## [v1.0.4] - 2019-07-23
### Code Refactoring
- change package name


<a name="v1.0.3"></a>
## [v1.0.3] - 2019-07-23
### Code Refactoring
- made easier creation of ConsulWatcher


<a name="v1.0.2"></a>
## [v1.0.2] - 2019-07-18

<a name="v1.0.1"></a>
## v1.0.1 - 2019-07-15

[Unreleased]: https://gitlab.com/shadowy/go/application-settings-consul/compare/v1.0.6...HEAD
[v1.0.6]: https://gitlab.com/shadowy/go/application-settings-consul/compare/v1.0.5...v1.0.6
[v1.0.5]: https://gitlab.com/shadowy/go/application-settings-consul/compare/v1.0.4...v1.0.5
[v1.0.4]: https://gitlab.com/shadowy/go/application-settings-consul/compare/v1.0.3...v1.0.4
[v1.0.3]: https://gitlab.com/shadowy/go/application-settings-consul/compare/v1.0.2...v1.0.3
[v1.0.2]: https://gitlab.com/shadowy/go/application-settings-consul/compare/v1.0.1...v1.0.2
